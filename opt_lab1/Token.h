#pragma once
#include <string>
#include <algorithm>

using namespace std;
class Token
{
public:
	Token(int, int, int, string);
	~Token();
	int getLine();
	int getPosition();
	int getCode();
	string getLexeme();

private:
	int line;
	int position;
	int code;
	string lexeme;
};



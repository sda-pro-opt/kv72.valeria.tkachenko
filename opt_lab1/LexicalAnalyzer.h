#pragma once
#include "Token.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>

using namespace std;

void LexicalAnalyzer(string);

int defineCategory(int);

bool checkIfcontains(string, map<string, int>);

bool checkIfKeyWord(string);

void executeLexicalAnalyze(string);
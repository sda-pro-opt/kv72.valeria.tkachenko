#include "LexicalAnalyzer.h"
#include "Tables.h"

string keyWords[6] = { "PROCEDURE", "BEGIN", "END", "LABEL", "GOTO", "RETURN" };

map<string, int> KW;
map<string, int> constants;
map<string, int> identifiers;
map<string, int> singleDelimeters;

vector<Token> lexemes;

void LexicalAnalyzer(string filename)
{
	size_t pos = filename.find('.');
	string newFilename = filename.substr(0, pos);

	ifstream programFile("tests/" + newFilename + "/" + filename);
	ofstream resultFile("tests/" + newFilename + "/" + "result " + newFilename + ".txt");

	int keyWordsCode = 401, identifiersCode = 1001, constantsCode = 501;

	int line = 1;
	int position = 1;

	char progChar = 0;
	int progCharCode = 0;

	char checkEof;

	programFile.get(progChar);
	progCharCode = (int)progChar;

	if (programFile.is_open())
	{
		cout << "File is succesfully opened\n" << endl;

		while (!programFile.eof())
		{			

			switch (defineCategory(progCharCode))
			{
				case 0:
				{
					if (progCharCode == 10)
					{
						line++; 
						position = 1;
					}
					else position++;

					programFile.get(progChar);
					progCharCode = (int)progChar;

					break;
				}

				case 1:
				{
					string lexema = "";

					while (defineCategory(progCharCode) == 1)
					{
						lexema += progChar;

						programFile.get(progChar);
						progCharCode = (int)progChar;						
					}
					
					if (defineCategory(progCharCode) == 2)
					{
						createError(1,line, position + lexema.length(), lexema);

						executeLexicalAnalyze("tests/" + newFilename + "/" + "result " + newFilename + ".txt");
						return;
					}

					if (!checkIfcontains(lexema, constants))
					{
						lexemes.push_back(Token(line, position, constantsCode, lexema));
						constants[lexema] = constantsCode++;
					}
					else
						lexemes.push_back(Token(line, position, constants[lexema], lexema));
					
					position += lexema.length();				


					break;
				}

				case 2:
				{
					string lexema = "";

					while (defineCategory(progCharCode) == 1 || defineCategory(progCharCode) == 2)
					{
						lexema += progChar;

						programFile.get(progChar);
						progCharCode = (int)progChar;
					}

					if (checkIfKeyWord(lexema)) 
					{
						lexemes.push_back(Token(line, position, keyWordsCode, lexema));
						KW[lexema] = keyWordsCode++;
					}

					else if (!checkIfcontains(lexema, identifiers)) 
					{
						lexemes.push_back(Token(line, position, identifiersCode++, lexema));
						identifiers[lexema] = identifiersCode;
					}
					else
						lexemes.push_back(Token(line, position, identifiers[lexema], lexema));
					
					position += lexema.length();					

					break;
				}
				
				case 3:
				{
					if (progCharCode == 40)
					{
						programFile.get(progChar);
						progCharCode = (int)progChar;
						position++;

						if (progCharCode == 42)
						{
							while (programFile.get(progChar))
							{
								if (progChar == '*')
								{
									programFile.get(progChar);
									if (progChar == ')')
									{
										programFile.get(progChar);
										progCharCode = (int)progChar;
										break;
									}
									else
									{
										createError(2, line, position, " " + progChar);

										executeLexicalAnalyze("tests/" + newFilename + "/" + "result " + newFilename + ".txt");
										return;
									}
								}

								if (progChar == '\n')
								{
									line++;
									position = 1;
								}
								
								position++;
							}
							
							break;
						}
						else 
						{
							lexemes.push_back(Token(line, position-1, '(', "("));
							singleDelimeters["("] = (int)'(';

							break;
						}
					}
					else 
					{
						string delimeter(1, progCharCode);
						lexemes.push_back(Token(line, position++, progCharCode, delimeter));
						singleDelimeters[delimeter] = progCharCode;

						programFile.get(progChar);
						progCharCode = (int)progChar;
						break;
					}

					break;
				}
				
				case 4:
				{
					string temp;
					temp.push_back(progChar);
					createError(0, line, position, " " + temp);

					executeLexicalAnalyze("tests/" + newFilename + "/" + "result " + newFilename + ".txt");
					return;
				}
			}

		}
	}
	else
		cout << "Error! Can't open file!";

	programFile.close();

	executeLexicalAnalyze("tests/" + newFilename + "/" + "result " + newFilename + ".txt");

}

int defineCategory(int charCode)
{
	if (charCode == 32 || charCode == 13 || charCode == 10 || charCode == 9)
	{
		return 0; //whitespaces
	}
	else if (charCode >= 48 && charCode <= 57)
	{
		return 1; //0-9
	}
	else if (charCode >= 65 && charCode <= 90)
	{
		return 2; //identifiers and key words
	}
	else if (charCode == 40 || charCode == 41 || charCode == 58	|| charCode == 59 || charCode == 44 || charCode == 36)
	{
		return 3; // single delimeters
	}
	else return 4; //invalid symbols
}

bool checkIfcontains(string lexema, map<string, int> container)
{
	return container[lexema] != NULL;
}


bool checkIfKeyWord(string str)
{
	
	for (int i = 0; i < 6; i++)
	{
		if (keyWords[i] == str)
		{
			return true;
		}

	}
	return false;
}

void executeLexicalAnalyze(string filename)
 {
	printKeyWordsTable(filename, KW, lexemes);
	printIdentifiersTable(filename, identifiers, lexemes);
	printConstantsTable(filename, constants, lexemes);
	printDelimetersTable(filename, singleDelimeters, lexemes);
	printError(filename);
}
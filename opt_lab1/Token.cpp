#include "Token.h"

Token::Token(int l, int p, int c, string lex)
{
	line = l;
	position = p;
	code = c;
	lexeme = lex;

}


int Token::getLine()
{
	return this->line;
}


int Token::getPosition()
{
	return this->position;
}


int Token::getCode()
{
	return this->code;
}

string Token::getLexeme()
{
	return this->lexeme;
}


Token::~Token()
{
}

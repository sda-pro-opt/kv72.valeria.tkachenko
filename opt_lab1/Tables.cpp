#include "Tables.h"

int errLine = -1;
int errPos = -1;
int errType = -1;
string errStr = "";

void printKeyWordsTable(string resFilename, map<string, int> keyWords, vector<Token> lexemes)
{
	ofstream resulFile(resFilename, ios::app);

	resulFile << "\nKEY WORDS TABLE:";
	for (auto& item : lexemes)
	{
		for (auto& KWitem : keyWords)
			if (KWitem.first == item.getLexeme())
			{
				resulFile << "\n " << item.getLexeme() << " code: " << item.getCode() << " line: "
					<< item.getLine() << " position: " << item.getPosition();
				
			}
	}

	resulFile.close();
}

void printIdentifiersTable(string resFilename, map<string, int> identifiers, vector<Token> lexemes)
{
	ofstream resulFile(resFilename, ios::app);

	resulFile << "\n\nIDENTIFIERS TABLE:";
	for (auto& item : lexemes)
	{
		for (auto& IDitem : identifiers)
			if (IDitem.first == item.getLexeme())
			{
				resulFile << "\n " << item.getLexeme() << " code: " << item.getCode() << " line: "
					<< item.getLine() << " position: " << item.getPosition();
				
			}

	}

	resulFile.close();
}

void printConstantsTable(string resFilename, map<string, int> constants, vector<Token> lexemes)
{
	ofstream resulFile(resFilename, ios::app);

	resulFile << "\n\nCONSTANTS TABLE:";
	for (auto& item : lexemes)
	{
		for (auto& CNitem : constants)
			if (CNitem.first == item.getLexeme())
			{
				resulFile << "\n " << item.getLexeme() << " code: " << item.getCode() << " line: "
					<< item.getLine() << " position: " << item.getPosition();
				
			}
	}

	resulFile.close();
}

void printDelimetersTable(string resFilename, map<string, int> delimeters, vector<Token> lexemes)
{
	ofstream resulFile(resFilename, ios::app);

	resulFile << "\n\n SINGLE DELIMETERS TABLE:";
	for (auto& item : lexemes)
	{
		for (auto& DLitem : delimeters)
			if (DLitem.first == item.getLexeme())
			{
				resulFile << "\n " << item.getLexeme() << " code: " << item.getCode() << " line: "
					<< item.getLine() << " position: " << item.getPosition();
				
			}
	}

	resulFile.close();
}

void createError(int erType, int line, int pos, string str)
{
	errLine = line;
	errPos = pos;
	errType = erType;
	errStr = str;
}

void printError(string resFilename)
{
	ofstream resulFile(resFilename, ios::app);
	
	if (errLine == -1 && errPos == -1)
		resulFile << "\n NO ERRORS DETECTED!\n";
	else if (errType == 1)
		resulFile << "\n Error detected (line " << errLine << " position " << errPos << "). Digit expected " << errStr << endl;
	else if (errType == 2)
		resulFile << "\n Error detected (line " << errLine << " position " << errPos << "). Unclosed comment " << endl;
	else
		resulFile << "\n Error detected (line " << errLine << " position " << errPos << "). Illegal symbol: " << errStr << endl;
	
	resulFile.close();
}
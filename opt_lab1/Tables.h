#pragma once
#include "Token.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

using namespace std;

void printKeyWordsTable(string, map<string, int>, vector<Token>);
void printIdentifiersTable(string, map<string, int>, vector<Token>);
void printConstantsTable(string, map<string, int>, vector<Token>);
void printDelimetersTable(string, map<string, int>, vector<Token>);
void printError(string);
void createError(int, int, int, string);

